﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
    <html>
    
      <teiheader>
        <fileDesc>
          <editionStmt align="center">
            <h1 style="font-style: bold; font-family: Old English Text MT;">Memorial Edition</h1>
          </editionStmt>
          <titleStmt>
            <h1 align="center">GREEK LEXIKON</h1>
            <h3 align="center"><lb/>OF THE</h3>
            <h1 align="center"><lb/>ROMAN AND BYZANTINE</h1>
            <h1 align="center"><lb/>PERIODS</h1>
            <h3 align="center"><lb/>(FROM B.C. 146 TO A.D. 1100)</h3>
            <h4 align="center" style="font-style: bold; font-family: GFS Porson;">Ἐ. Ἀ. Σοφοκλῆς</h4>
            <h4 align="center">E. A. SOPHOCLES</h4>
            <h4 align="center">By</h4>
            <h4 align="center">E. A. SOPHOCLES</h4>
            <h4 align="center">LATE UNIVERSITY PROFESSOR OF ANCIENT, BYZANTINE, AND MODERN GREEK IN HARVARD UNIVERSITY</h4>
          </titleStmt>
          <publicationStmt align="center">
            <h4 align="center">CAMBRIDGE</h4>
            <h4 align="center">HARVARD UNIVERSITY PRESS</h4>
            <h4 align="center">LONDON</h4>
            <h4 align="center">HUMPHREY MILFORD
                          OXFORD UNIVERSITY PRESS</h4>
            <h4 align="center">1914</h4>
            <pb/>
            <h5 align="center">Entered according to Act of Congress, in the year 1870, by
              E. A. SOPHOCLES,
              In the Clerk’s Office of the District Court of the District of Massachusetts.</h5>
          </publicationStmt>
          <h5 align="center">UNIVERSITY PRESS: JOHN WILSON AND SON, CAMBRIDGE, U.S.A.</h5>
        <sourceDesc>
        <h6 align="center">https://anemi.lib.uoc.gr/metadata/f/5/3/metadata-01-0001130.tkl</h6>
      </sourceDesc>
      
        </fileDesc>
        <lb/>
        <profileDesc>
          <langUsage>
            <header align="center" type="color: black">
              <lb/>
              <h4>Used Languages:</h4>
            </header>
            <language id="lat" align="center" type="color: black">
              <p>Latin,</p>
            </language>
            <language id="eng" align="center" type="color: black">
              <p>English,</p>
            </language>
            <language id="grc" align="center" type="color: black">
              <p>Greek, Ancient (to 1453),</p>
            </language>
            <language id="heb" align="center" type="color: black">
              <p>Hebrew</p>
            </language>
          </langUsage>
        </profileDesc>
      </teiheader>
      <pb/>
      <page align="center">
        <h5><lb/>p.57</h5>
      </page>
      <titlePart align="center">
        <h3 align="center"><lb/>LEXICON.</h3>
      </titlePart>
      
      <front>
        <div style="max-width:600px; margin: 0px auto; x-column-count: 2; x-column-gap: 40px;" align="left" type="color: purple">
          <xsl:apply-templates select="tei/text/front"/>
          <xsl:apply-templates/>
        </div>
        
      </front>
      <p>        
        <div type="entry"/>
      </p>
      <cb n="1"/>      
      <body>
        <p>
          <div type="letter" n="A">
            <head align="center">
              <h4 align="center" style="font-family: GFS Porson; color: purple; font-style: bold">A</h4>
            </head>
          </div>
        </p>
        <div style="max-width:600px; margin: 0px auto; x-column-count: 2; x-column-gap: 40px;">
          <xsl:apply-templates select="tei/text/body/*"/>
          <xsl:apply-templates/>
        </div>
        <xsl:apply-templates/>
      </body>
      <pb/>
      <page align="center">
        <h5><lb/>p.74</h5>
      </page>
      
    </html>
  </xsl:template>
  
  
  <xsl:template match="//entry"> 
    <span style="display: block; line-height: 1.5em; margin: 1.5em 0 1.5em -1em; text-indent: -1em;" xml:space="preserve">      
	<xsl:apply-templates />
	</span>
  </xsl:template>
   
 <!--a default <form>--> 
 <xsl:template match="//form">
    <span style="font-style: bold" xml:space="preserve">      
	<xsl:apply-templates />
	</span>
  </xsl:template>
   
<!--a default <orth>-->
 <xsl:template match="//orth">
    <span style="font-family: GFS Porson; color: red; font-style: bold ">
      <xsl:apply-templates />
    </span>
    <xsl:if test="following-sibling::node()[1][name()='orth']">
      <xsl:text></xsl:text>
      <span style="color: #999999">|</span>
      <xsl:text></xsl:text>
    </xsl:if>
  </xsl:template>
  
 <!--a default <lbl>-->
 <xsl:template match="//lbl|">
    <span style="font-family: GFS Porson; color: blue; font-style: bold ">
      <xsl:apply-templates />
    </span>
    <xsl:if test="following-sibling::node()[1][name()='lbl']">
      <xsl:text></xsl:text>
      <span style="color: #999999">|</span>
      <xsl:text></xsl:text>
    </xsl:if>
  </xsl:template> 
  
<!--a default <etym>-->
 <xsl:template match="//etym|">
    <span style="font-family: GFS Porson; color: green; font-style: bold ">
      <xsl:apply-templates />
    </span>
    <xsl:if test="following-sibling::node()[1][name()='etym']">
      <xsl:text></xsl:text>
      <span style="color: #999999">|</span>
      <xsl:text></xsl:text>
    </xsl:if>
  </xsl:template> 
  
    <!--a default <def>-->
  <xsl:template match="//def">
    <xsl:apply-templates />
        <xsl:if test="following-sibling::*[1][name()='def' and (@type='translation' or @type='trans')]">
          <xsl:text></xsl:text>
      <span style="font-family: Courier New ; color: blue">|</span>
      <xsl:text></xsl:text>
    </xsl:if>
  </xsl:template>
  
  <!--a default <note>-->
  <xsl:template match="//note">
    <xsl:apply-templates />
    <xsl:if test="following-sibling::*[1][name()='note' and (@type='translation' or @type='trans')]">
      <xsl:text></xsl:text>
      <span style="font-family: Times New Roman; color: blue">|</span>
      <xsl:text></xsl:text>
    </xsl:if>
  </xsl:template>
  
  
<!--a default <c>-->
  <xsl:template match="//c">
    <span style="font-family: Arial Black; color: green; font-style: bold">
      <xsl:apply-templates />
    </span>
  </xsl:template>  
 
 
 <!--a default <number>-->
  <xsl:template match="//number">
    <span style="font-family: Arial Black; color: black; font-style: bold">
      <xsl:apply-templates />
    </span>
  </xsl:template>
  
   
 <!--a default <colloc>-->
  <xsl:template match="//colloc">
    <span style="font-family: GFS Porson; color: black; font-style: italics; font-style: bold">|</span>
      <xsl:apply-templates />
  </xsl:template> 

  <!--a default <cit>-->
  <xsl:template match="//cit">
    <span style="font-family: GFS Porson; color: black; font-style: italics; font-style: bold">|</span>
      <xsl:apply-templates />
    
  </xsl:template>
  
  
   <!--various labels-->
  <xsl:template match="//usg|//lang">
    <span style="color: dark grey; font-style: italics; font-style: bold">
      <xsl:apply-templates />
    </span>
  </xsl:template>
  
 <!--various references-->
  <xsl:template match="//ref">
    <span style="font-family: Mistral color: Green; font-style: italics">
      <xsl:apply-templates />
    </span>
  </xsl:template>
  
</xsl:stylesheet>
